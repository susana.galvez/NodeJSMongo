"use strict";

function escribeTras2Segundos(texto,callback){
    setTimeout(function(){
console.log(texto)
callback();
    },2000)
}
//Bucle asíncroo en serie
//Llamar a una función con un ARRAY en serie y 
// al finalizar llamar al callback de finalización
function serie(lista,func,callbackFin){
    if(lista.length==0){
        callbackFin();
        return;
    }
    
    func(lista.shift(),function(){
        serie(lista,func,callbackFin)
    })
}


var lista =[1,2,"tres",4,5]
serie(lista,escribeTras2Segundos,function(){
    console.log('He terminado')
})
