"use strict";

var casa={
    ventanas:2,
    cuantasVentanas:function(){
        console.log("La casa tiene " + this.ventanas + " ventanas")
    }
};
casa.cuantasVentanas();
setTimeout(casa.cuantasVentanas.bind(casa),1000)


function Coche(){
    this.ruedas=4,
    this.cuantasRuedas=function(){
        console.log("El coche tiene " + this.ruedas + " ruedas")
    }
};
var coche = new Coche()
coche.cuantasRuedas()

setTimeout(coche.cuantasRuedas.bind(coche),1000);
//en esta segunda función salia undefined y 
//al ponerle el bind, ya muestra lo real

