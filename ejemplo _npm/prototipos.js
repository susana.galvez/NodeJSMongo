"use strict";
function Persona(nombre) {
    this.name = nombre;
}
var persona = new Persona('Neo')

Persona.prototype.saluda = function(){
    console.log(" Hola, me llamo " + this.name)
}
console.log(persona.name)
persona.saluda()

//---------------------------------Heredando de Persona-----------------------//

function Agente(nombre){
    Persona.call(this, nombre)
//Esto ejecuta el constructor de Persona con el this de Agente
//definiendo en el this de Agente una propiedad name
// y asignandole el parámetro recibido
}

Agente.prototype= new Persona('Soy un prototipo')

var agente = new Agente('Smith')
agente.saluda();
console.log(
    Object.getPrototypeOf(agente),
    agente instanceof Agente,
    agente instanceof Persona,
    agente instanceof Object
)

// ----------Herencia Múltiple-----------------------------------------------//

function SuperHeroe(){
    this.vuela=function(){
        console.log(this.name + " vuela ")
    }
    this.esquivaBalas=function(){
        console.log(this.name + " esquivaBalas ")
    }
}
//Le asigna al agente las funciones del SuperHeroe
Object.assign(Agente.prototype, new SuperHeroe)

agente.vuela()
agente.esquivaBalas()