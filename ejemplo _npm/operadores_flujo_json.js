//definimos un objeto de forma literal

var empleado={
nombre:"Susana",
profesión:"Dev",
edad: 35,
armas:["ordenador","raton", "pantalla","ingles"]
};

console.log(empleado)

var cambiarObjeto= JSON.stringify(empleado);
console.log(cambiarObjeto)

// combertimos el estring a objeto

var convertirObjetoAString= JSON.parse(cambiarObjeto)
console.log(convertirObjetoAString)

for(var i=0; i<empleado.armas.length;i++){
    var arma=empleado.armas[i];
    console.log(empleado.nombre + (arma==' ingles '? "sabe": ' tiene ') + arma)
}