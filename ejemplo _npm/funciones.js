//metodo de un objeto
var agente={
    nombre:"susana",
    saluda:function()
    {console.log("Hola soy "+ this.nombre)}
};
agente.saluda()

function Fruta(nombre){
    this.getNombre = function(){
        return nombre
    }
    this.setNombre = function(valor){
        nombre=valor;
}
}
//Crear un objeto fruta
var limon = new Fruta('Limon')
console.log(limon)
console.log(limon.getNombre())

limon.setNombre('Manzana')
console.log(limon.getNombre())

